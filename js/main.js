var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Upgrades = (function () {
    function Upgrades(cost, distancePerClick) {
        this._cost = cost;
        this._distancePerClick1 = distancePerClick;
        this._distancePerClick2 = distancePerClick;
        this._purchased1 = false;
        this._purchased2 = false;
    }
    Object.defineProperty(Upgrades.prototype, "distancePerClick1", {
        get: function () {
            return this._distancePerClick1;
        },
        set: function (distance) {
            this._distancePerClick1 += distance;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Upgrades.prototype, "distancePerClick2", {
        get: function () {
            return this._distancePerClick2;
        },
        set: function (distance) {
            this._distancePerClick2 += distance;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Upgrades.prototype, "purchased1", {
        get: function () {
            return this._purchased1;
        },
        set: function (purchased) {
            this._purchased1 = purchased;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Upgrades.prototype, "purchased2", {
        get: function () {
            return this._purchased2;
        },
        set: function (purchased) {
            this._purchased2 = purchased;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Upgrades.prototype, "cost", {
        get: function () {
            return this._cost;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Upgrades.prototype, "name", {
        get: function () {
            return this._name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Upgrades.prototype, "activateP1", {
        get: function () {
            return this._activateKey1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Upgrades.prototype, "activateP2", {
        get: function () {
            return this._activateKey2;
        },
        enumerable: true,
        configurable: true
    });
    return Upgrades;
}());
var AeroBike = (function (_super) {
    __extends(AeroBike, _super);
    function AeroBike(cost, distancePerClick) {
        var _this = _super.call(this, cost, distancePerClick) || this;
        _this._name = "Aerobike";
        _this._activateKey1 = "3";
        _this._activateKey2 = "9";
        return _this;
    }
    return AeroBike;
}(Upgrades));
var AeroWheels = (function (_super) {
    __extends(AeroWheels, _super);
    function AeroWheels(cost, distancePerClick) {
        var _this = _super.call(this, cost, distancePerClick) || this;
        _this._name = "AeroWheels";
        _this._activateKey1 = "1";
        _this._activateKey2 = "7";
        return _this;
    }
    return AeroWheels;
}(Upgrades));
var Bike = (function () {
    function Bike() {
        this._bike1Div = document.getElementById("bike1");
        this._bike2Div = document.getElementById("bike2");
        this._bike1 = "Bike.png";
        this._bikeOther1 = "bike-andersom.png";
        this._bike2 = "Bike.png";
        this._bikeOther2 = "bike-andersom.png";
        this._wheels1 = "wielen.png";
        this._wheels2 = "wielen.png";
    }
    Bike.prototype.setBike = function (player, upgrade) {
        if (player == 1) {
            switch (upgrade) {
                case "Aerobike":
                    this._bike1 = "Aerobike.png";
                    this._bikeOther1 = "aerobike-andersom.png";
                    break;
                case "Ttbike":
                    this._bike1 = "Ttbike.png";
                    this._bikeOther1 = "ttbike-andersom.png";
                    break;
                default:
                    this._bike1 = "Bike.png";
                    this._bikeOther1 = "bike-andersom.png";
                    break;
            }
        }
        else {
            switch (upgrade) {
                case "Aerobike":
                    this._bike2 = "Aerobike.png";
                    this._bikeOther2 = "aerobike-andersom.png";
                    break;
                case "Ttbike":
                    this._bike2 = "Ttbike.png";
                    this._bikeOther2 = "ttbike-andersom.png";
                    break;
                default:
                    this._bike2 = "Bike.png";
                    this._bikeOther2 = "bike-andersom.png";
                    break;
            }
        }
        console.log(this._bike1);
    };
    Bike.prototype.rotateBike = function (keyCode) {
        switch (keyCode) {
            case 65:
                this._bike1Div.innerHTML = "\n                <img id=\"wheels\" class=\"bikeImage\" src=\"./assets/images/" + this._wheels1 + "\">\n                <img id=\"bikeImage\" class=\"bikeImage\" src=\"./assets/images/" + this._bikeOther1 + "\">\n                ";
                break;
            case 68:
                this._bike1Div.innerHTML = "\n                <img id=\"wheels\" class=\"bikeImage\" src=\"./assets/images/" + this._wheels1 + "\">\n                <img id=\"bikeImage\" class=\"bikeImage\" src=\"./assets/images/" + this._bike1 + "\">\n                ";
                break;
            case 37:
                this._bike2Div.innerHTML = "\n                <img id=\"wheels\" class=\"bikeImage\" src=\"./assets/images/" + this._wheels2 + "\">\n                <img id=\"bikeImage\" class=\"bikeImage\" src=\"./assets/images/" + this._bikeOther2 + "\">\n                ";
                break;
            case 39:
                this._bike2Div.innerHTML = "\n                <img id=\"wheels\" class=\"bikeImage\" src=\"./assets/images/" + this._wheels2 + "\">                \n                <img id=\"bikeImage\" class=\"bikeImage\" src=\"./assets/images/" + this._bike2 + "\">\n                ";
                break;
            default:
                break;
        }
        console.log(this._wheels1);
    };
    Bike.prototype.setWheels = function (player, upgrade) {
        if (player == 1) {
            switch (upgrade) {
                case "AeroWheels":
                    this._wheels1 = "AeroWheels.png";
                    break;
                case "TtWheels":
                    this._wheels1 = "TtWheels.png";
                    break;
            }
        }
        else {
            switch (upgrade) {
                case "AeroWheels":
                    this._wheels2 = "AeroWheels.png";
                    break;
                case "TtWheels":
                    this._wheels2 = "TtWheels.png";
                    break;
            }
        }
    };
    return Bike;
}());
var Game = (function () {
    function Game() {
        var _this = this;
        this.keyDownHandler = function (e) {
            switch (e.keyCode) {
                case 37:
                    if (_this._position2) {
                        _this.count(2);
                        _this._playfield.moveBike(e.keyCode);
                        _this._position2 = false;
                    }
                    break;
                case 39:
                    if (!_this._position2) {
                        _this.count(2);
                        _this._playfield.moveBike(e.keyCode);
                        _this._position2 = true;
                    }
                    break;
                case 65:
                    if (_this._position1) {
                        _this.count(1);
                        _this._playfield.moveBike(e.keyCode);
                        _this._position1 = false;
                    }
                    break;
                case 68:
                    if (!_this._position1) {
                        _this.count(1);
                        _this._playfield.moveBike(e.keyCode);
                        _this._position1 = true;
                    }
                    break;
                case 37 && 65:
                    if (_this._position2) {
                        _this.count(2);
                        _this._playfield.moveBike(37);
                        _this._position2 = false;
                    }
                    if (_this._position1) {
                        _this.count(1);
                        _this._playfield.moveBike(65);
                        _this._position1 = false;
                    }
                    break;
                case 39 && 65:
                    if (!_this._position2) {
                        _this.count(2);
                        _this._playfield.moveBike(39);
                        _this._position2 = true;
                    }
                    if (_this._position1) {
                        _this.count(1);
                        _this._playfield.moveBike(65);
                        _this._position1 = false;
                    }
                    break;
                case 37 && 68:
                    if (!_this._position2) {
                        _this.count(2);
                        _this._playfield.moveBike(37);
                        _this._position2 = false;
                    }
                    if (!_this._position1) {
                        _this.count(1);
                        _this._playfield.moveBike(68);
                        _this._position1 = true;
                    }
                    break;
                case 39 && 68:
                    if (!_this._position2) {
                        _this.count(2);
                        _this._playfield.moveBike(39);
                        _this._position2 = true;
                    }
                    if (!_this._position1) {
                        _this.count(1);
                        _this._playfield.moveBike(68);
                        _this._position1 = true;
                    }
                    break;
                default:
                    _this._playfield.upgradeContainer.buyItem(e, _this._distance1, _this._distance2);
                    break;
            }
            _this.setCounter(_this._distance1, _this._distance2);
            _this.winChecker();
        };
        this._playfield = new PlayField();
        this._position1 = true;
        this._position2 = true;
        this._started = false;
        this._distance1 = 0;
        this._distance2 = 0;
        this._maxScore = 10000;
        window.addEventListener("keydown", this.keyDownHandler);
    }
    Game.prototype.winChecker = function () {
        if (this._distance1 >= this._maxScore) {
            document.getElementById("container").innerHTML = "\n            <h1 style=\"font-weight: bold; color: #019DFD; font-size: 400%; margin-left: calc(50% - 200px); margin-top: 20vh; \">\n                Player 1 heeft gewonnen\n            </h1>\n            ";
        }
        if (this._distance2 >= this._maxScore) {
            document.getElementById("container").innerHTML = "\n            <h1 style=\"font-weight: bold; color: #019DFD; font-size: 400%; margin-left: calc(50% - 200px); margin-top: 20vh; \">\n                Player 2 heeft gewonnen\n            </h1>\n            ";
        }
    };
    Game.prototype.count = function (player) {
        if (player == 1) {
            this._distance1 += this._playfield.upgradeContainer.getDistanceOnClick1;
        }
        else {
            this._distance2 += this._playfield.upgradeContainer.getDistanceOnClick2;
        }
    };
    Game.prototype.setCounter = function (player1, player2) {
        var count1 = document.getElementById("count1");
        count1.innerHTML = player1 + " meter";
        var count2 = document.getElementById("count2");
        count2.innerHTML = player2 + " meter";
    };
    return Game;
}());
var app;
(function () {
    var init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
var PlayField = (function () {
    function PlayField() {
        this._bike = new Bike();
        this._upgradeContainer = new UpgradeContainer();
    }
    PlayField.prototype.moveBike = function (code) {
        this.changeBike();
        this._bike.rotateBike(code);
    };
    PlayField.prototype.changeBike = function () {
        for (var i = 0; i < this._upgradeContainer.upgrades.length; i++) {
            if (this._upgradeContainer.upgrades[i].purchased1) {
                this._bike.setBike(1, this._upgradeContainer.upgrades[i].name);
                this._bike.setWheels(1, this._upgradeContainer.upgrades[i].name);
                console.log(this._upgradeContainer.upgrades[i].name);
            }
            if (this._upgradeContainer.upgrades[i].purchased2) {
                this._bike.setBike(2, this._upgradeContainer.upgrades[i].name);
                this._bike.setWheels(2, this._upgradeContainer.upgrades[i].name);
            }
            if (this._upgradeContainer.upgrades[i].purchased1 && this._upgradeContainer.upgrades[i].purchased2) {
                this._bike.setBike(1, this._upgradeContainer.upgrades[i].name);
                this._bike.setBike(2, this._upgradeContainer.upgrades[i].name);
                this._bike.setWheels(1, this._upgradeContainer.upgrades[i].name);
                this._bike.setWheels(2, this._upgradeContainer.upgrades[i].name);
            }
        }
    };
    Object.defineProperty(PlayField.prototype, "upgradeContainer", {
        get: function () {
            return this._upgradeContainer;
        },
        enumerable: true,
        configurable: true
    });
    return PlayField;
}());
var Ttbike = (function (_super) {
    __extends(Ttbike, _super);
    function Ttbike(cost, distancePerClick) {
        var _this = _super.call(this, cost, distancePerClick) || this;
        _this._name = "Ttbike";
        _this._activateKey1 = "4";
        _this._activateKey2 = "0";
        return _this;
    }
    return Ttbike;
}(Upgrades));
var TtWheels = (function (_super) {
    __extends(TtWheels, _super);
    function TtWheels(cost, distancePerClick) {
        var _this = _super.call(this, cost, distancePerClick) || this;
        _this._name = "TtWheels";
        _this._activateKey1 = "2";
        _this._activateKey2 = "8";
        return _this;
    }
    return TtWheels;
}(Upgrades));
var UpgradeContainer = (function () {
    function UpgradeContainer() {
        this._upgrades = [];
        this._aerowheels = new AeroWheels(100, 5);
        this._ttwheels = new TtWheels(250, 20);
        this._aerobike = new AeroBike(2000, 100);
        this._ttbike = new Ttbike(3000, 200);
        this._upgrades.push(this._aerowheels);
        this._upgrades.push(this._ttwheels);
        this._upgrades.push(this._aerobike);
        this._upgrades.push(this._ttbike);
        this.createDom();
    }
    UpgradeContainer.prototype.buyItem = function (e, distanceP1, distanceP2) {
        if (e.key == "1" || "2" || "3" || "4") {
            for (var i = 0; i < this._upgrades.length; i++) {
                if (this._upgrades[i].activateP1 == e.key) {
                    if (distanceP1 >= this._upgrades[i].cost) {
                        this._upgrades[i].purchased1 = true;
                        console.log(this._upgrades[i].name + this._upgrades[i].purchased1);
                    }
                }
            }
        }
        if (e.key == "7" || "8" || "9" || "0") {
            for (var i = 0; i < this._upgrades.length; i++) {
                if (this._upgrades[i].activateP2 == e.key) {
                    if (distanceP2 >= this._upgrades[i].cost) {
                        this._upgrades[i].purchased2 = true;
                        console.log(this._upgrades[i].purchased2);
                    }
                }
            }
        }
        this.updateUpgradeCard();
    };
    UpgradeContainer.prototype.updateUpgradeCard = function () {
        for (var i = 0; i < this._upgrades.length; i++) {
            if (this._upgrades[i].purchased1) {
                var card = document.getElementById(this._upgrades[i].name + 1);
                card.style.backgroundColor = "green";
                card.style.color = "white";
            }
            if (this._upgrades[i].purchased2) {
                var card = document.getElementById(this._upgrades[i].name + 2);
                card.style.backgroundColor = "green";
                card.style.color = "white";
            }
            if (this._upgrades[i].purchased1 && this._upgrades[i].purchased2) {
                var card1 = document.getElementById(this._upgrades[i].name + 1);
                card1.style.backgroundColor = "green";
                card1.style.color = "white";
                var card2 = document.getElementById(this._upgrades[i].name + 2);
                card2.style.backgroundColor = "green";
                card2.style.color = "white";
            }
        }
    };
    UpgradeContainer.prototype.createDom = function () {
        for (var i = 0; i < this._upgrades.length; i++) {
            console.log(this._upgrades[i]);
            console.log(this._upgrades[i].name);
            document.getElementById("player1").innerHTML += "\n                <div class=\"buyCard\" id=\"" + (this._upgrades[i].name + 1) + "\">\n                    <img class=\"upgradeImage\" src=\"./assets/images/" + this._upgrades[i].name + ".png\" alt=\"temp\">\n                    <div class=\"upgradeText\">\n                        <h2 class=\"upgradeName\"> " + this._upgrades[i].name + "</h2>\n                        <p><b>Cost: </b>" + this._upgrades[i].cost + " meters</p>\n                        <p>Press " + this._upgrades[i].activateP1 + " to buy</P>\n                    </div>\n                </div>\n            ";
            document.getElementById("player2").innerHTML += "\n                <div class=\"buyCard\" id=\"" + (this._upgrades[i].name + 2) + "\">\n                    <img class=\"upgradeImage\" src=\"./assets/images/" + this._upgrades[i].name + ".png\" alt=\"temp\">\n                    <div class=\"upgradeText\">\n                        <h2 class=\"upgradeName\"> " + this._upgrades[i].name + "</h2>\n                        <p><b>Cost: </b>" + this._upgrades[i].cost + " meters</p>\n                        <p>Press " + this._upgrades[i].activateP2 + " to buy</P>\n                    </div>\n                </div>\n            ";
        }
    };
    Object.defineProperty(UpgradeContainer.prototype, "getDistanceOnClick1", {
        get: function () {
            var returnValue = 1;
            for (var i = 0; i < this._upgrades.length; i++) {
                if (this._upgrades[i].purchased1) {
                    returnValue += this._upgrades[i].distancePerClick1;
                }
            }
            return returnValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpgradeContainer.prototype, "getDistanceOnClick2", {
        get: function () {
            var returnValue = 1;
            for (var i = 0; i < this._upgrades.length; i++) {
                if (this._upgrades[i].purchased2) {
                    returnValue += this._upgrades[i].distancePerClick2;
                }
            }
            return returnValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpgradeContainer.prototype, "upgrades", {
        get: function () {
            return this._upgrades;
        },
        enumerable: true,
        configurable: true
    });
    return UpgradeContainer;
}());
//# sourceMappingURL=main.js.map