/// <reference path="Upgrades.ts" />

class AeroBike extends Upgrades{

    constructor(cost:number, distancePerClick:number) {
        super(cost, distancePerClick);
        this._name = "Aerobike"; 
        this._activateKey1 = "3";
        this._activateKey2 = "9";
    }
}