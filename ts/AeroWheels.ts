/// <reference path="Upgrades.ts" />

class AeroWheels extends Upgrades{
    constructor(cost:number, distancePerClick:number) {
        super(cost,distancePerClick);
        this._name = "AeroWheels";
        this._activateKey1 = "1";
        this._activateKey2 = "7";
    }
}