class Bike {
    private _bike1Div: HTMLElement;
    private _bike2Div: HTMLElement;
    private _bike1: string;
    private _bikeOther1: string;
    private _bike2: string;
    private _bikeOther2: string;
    private _wheels1: string;
    private _wheels2: string;

    constructor() {
        this._bike1Div = document.getElementById("bike1");
        this._bike2Div = document.getElementById("bike2");
        this._bike1 = "Bike.png";   //setting up standard bike for player 1
        this._bikeOther1 = "bike-andersom.png"; //setting up standard bike for player 1
        this._bike2 = "Bike.png"; //setting up standard bike for player 2
        this._bikeOther2 = "bike-andersom.png"; //setting up standard bike for player 2
        this._wheels1 = "wielen.png"; //setting up standard wheels for player 1
        this._wheels2 = "wielen.png"; //setting up standard wheels for player 2
    }

    public setBike(player:number, upgrade: string){ //set bikes when upgrades are bought
        if(player == 1){
            switch (upgrade) {
                case "Aerobike":
                    this._bike1 = "Aerobike.png";
                    this._bikeOther1 = "aerobike-andersom.png"
                    break;

                case "Ttbike":
                    this._bike1 = "Ttbike.png";
                    this._bikeOther1 = "ttbike-andersom.png"
                    break;

                default:
                    this._bike1 = "Bike.png";
                    this._bikeOther1 = "bike-andersom.png"
                    break;
            }
        } else {
            switch (upgrade) {
                case "Aerobike":
                    this._bike2 = "Aerobike.png";
                    this._bikeOther2 = "aerobike-andersom.png"
                    break;

                case "Ttbike":
                    this._bike2 = "Ttbike.png";
                    this._bikeOther2 = "ttbike-andersom.png"
                    break;
            
                default:
                    this._bike2 = "Bike.png";
                    this._bikeOther2 = "bike-andersom.png"
                    break;
            }
        }
        console.log(this._bike1);
    }

    public rotateBike(keyCode: number){ //rotates pedals through changing pictures
        switch (keyCode) {
            case 65:
                this._bike1Div.innerHTML = `
                <img id="wheels" class="bikeImage" src="./assets/images/${this._wheels1}">
                <img id="bikeImage" class="bikeImage" src="./assets/images/${this._bikeOther1}">
                `;
                break;
            
            case 68:
                this._bike1Div.innerHTML = `
                <img id="wheels" class="bikeImage" src="./assets/images/${this._wheels1}">
                <img id="bikeImage" class="bikeImage" src="./assets/images/${this._bike1}">
                `;
                break;

            case 37:
                this._bike2Div.innerHTML = `
                <img id="wheels" class="bikeImage" src="./assets/images/${this._wheels2}">
                <img id="bikeImage" class="bikeImage" src="./assets/images/${this._bikeOther2}">
                `;
                break;

            case 39:
                this._bike2Div.innerHTML = `
                <img id="wheels" class="bikeImage" src="./assets/images/${this._wheels2}">                
                <img id="bikeImage" class="bikeImage" src="./assets/images/${this._bike2}">
                `
                break;


            default:
                break;
                
        }
        console.log(this._wheels1);
    }

    public setWheels(player: number, upgrade: string){ //setting up the wheels
        if(player == 1){
            switch (upgrade) {
                case "AeroWheels":
                    this._wheels1 = "AeroWheels.png";
                    break;

                case "TtWheels":
                    this._wheels1 = "TtWheels.png";
                    break;

            }
        } else {
            switch (upgrade) {
                case "AeroWheels":
                    this._wheels2 = "AeroWheels.png";
                    break;

                case "TtWheels":
                    this._wheels2 = "TtWheels.png";
                    break;

            }
        }
    }
}