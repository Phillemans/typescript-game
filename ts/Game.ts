class Game {

    private _playfield: PlayField;
    private _position1: boolean;
    private _position2: boolean;
    private _started: boolean;
    private _distance1: number;
    private _distance2: number;
    private _maxScore: number;

    constructor() {
        this._playfield = new PlayField();
        this._position1 = true;
        this._position2 = true;
        this._started = false;
        this._distance1 = 0;
        this._distance2 = 0;
        this._maxScore = 10000;
        window.addEventListener("keydown", this.keyDownHandler);
    }

    public winChecker(){ //checkt of er al door iemand gewonnen is
        if(this._distance1 >= this._maxScore){
            document.getElementById("container").innerHTML = `
            <h1 style="font-weight: bold; color: #019DFD; font-size: 400%; margin-left: calc(50% - 200px); margin-top: 20vh; ">
                Player 1 heeft gewonnen
            </h1>
            `;
        }
        if(this._distance2 >= this._maxScore){
            document.getElementById("container").innerHTML = `
            <h1 style="font-weight: bold; color: #019DFD; font-size: 400%; margin-left: calc(50% - 200px); margin-top: 20vh; ">
                Player 2 heeft gewonnen
            </h1>
            `;
        }
    }

    public keyDownHandler = (e: KeyboardEvent) => { //kijkt welke toets wordt ingedrukt
        switch (e.keyCode) {
            case 37:
                if (this._position2) {
                    this.count(2);
                    this._playfield.moveBike(e.keyCode);
                    this._position2 = false;//zorgt dat niet twee keer links geklikt kan worden
                }
                break;
            case 39:
                if (!this._position2) {
                    this.count(2);
                    this._playfield.moveBike(e.keyCode);
                    this._position2 = true; //zorgt dat niet twee keer rechts geklikt kan worden
                }
                break;
            case 65:
                if (this._position1) {
                    this.count(1);
                    this._playfield.moveBike(e.keyCode);
                    this._position1 = false;
                }
                break;
            case 68:
                if (!this._position1) {
                    this.count(1);
                    this._playfield.moveBike(e.keyCode);
                    this._position1 = true;//zorgt dat niet twee keer rechts geklikt kan worden
                }
                break;

            case 37 && 65:
                if (this._position2) {
                    this.count(2);
                    this._playfield.moveBike(37);
                    this._position2 = false;
                }
                if (this._position1) {
                    this.count(1);
                    this._playfield.moveBike(65);
                    this._position1 = false;
                }
                break;

            case 39 && 65:
                if (!this._position2) {
                    this.count(2);
                    this._playfield.moveBike(39);
                    this._position2 = true;
                }
                if (this._position1) {
                    this.count(1);
                    this._playfield.moveBike(65);
                    this._position1 = false;
                }
                break;

            case 37 && 68:
                if (!this._position2) {
                    this.count(2);
                    this._playfield.moveBike(37);
                    this._position2 = false
                }
                if (!this._position1) {
                    this.count(1);
                    this._playfield.moveBike(68);
                    this._position1 = true;
                }
                break;

            case 39 && 68:
                if (!this._position2) {
                    this.count(2);
                    this._playfield.moveBike(39);
                    this._position2 = true;
                }
                if (!this._position1) {
                    this.count(1);
                    this._playfield.moveBike(68);
                    this._position1 = true;
                }
                break;
            
            default:
                this._playfield.upgradeContainer.buyItem(e, this._distance1, this._distance2);
                break;
        }
        this.setCounter(this._distance1, this._distance2);
        this.winChecker();
    }

    public count(player: number) {
        if (player == 1) {
            this._distance1 += this._playfield.upgradeContainer.getDistanceOnClick1; //veranderd de afstand
        } else {
            this._distance2 += this._playfield.upgradeContainer.getDistanceOnClick2; //verrander de afstand
        }
        // console.log("Distance1: " + this._distance1);
        // console.log("Distance2: " + this._distance2);
    }

    public setCounter(player1: number, player2: number) {
        let count1 = document.getElementById("count1");
        count1.innerHTML = player1 + " meter"; //veranderd cijfer in html
        let count2 = document.getElementById("count2");
        count2.innerHTML = player2 + " meter"; //veranderd cijfer in html
    }
}