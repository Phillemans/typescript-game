class PlayField {

        private _bike: Bike;
        private _upgradeContainer: UpgradeContainer;

    constructor(){

        this._bike = new Bike();
        this._upgradeContainer = new UpgradeContainer();
        
    }

    public moveBike(code: number){ //rotates the pedals of the bike
        this.changeBike();
        this._bike.rotateBike(code);
    }

    public changeBike(){ //changes bike and wheels if upgraded 
        for(let i = 0; i < this._upgradeContainer.upgrades.length; i ++){
            if(this._upgradeContainer.upgrades[i].purchased1){
                this._bike.setBike(1, this._upgradeContainer.upgrades[i].name);
                this._bike.setWheels(1, this._upgradeContainer.upgrades[i].name);
                console.log(this._upgradeContainer.upgrades[i].name);
            }
            if(this._upgradeContainer.upgrades[i].purchased2){
                this._bike.setBike(2, this._upgradeContainer.upgrades[i].name);
                this._bike.setWheels(2, this._upgradeContainer.upgrades[i].name);
            }
            if(this._upgradeContainer.upgrades[i].purchased1 && this._upgradeContainer.upgrades[i].purchased2){
                this._bike.setBike(1, this._upgradeContainer.upgrades[i].name);
                this._bike.setBike(2, this._upgradeContainer.upgrades[i].name);
                this._bike.setWheels(1, this._upgradeContainer.upgrades[i].name);
                this._bike.setWheels(2, this._upgradeContainer.upgrades[i].name);
            }
        }
    }

    get upgradeContainer(){
        return this._upgradeContainer
    }
}