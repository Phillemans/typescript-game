/// <reference path="Upgrades.ts" />

class TtWheels extends Upgrades{
    constructor(cost:number, distancePerClick:number) {
        super(cost,distancePerClick);
        this._name = "TtWheels";
        this._activateKey1 = "2";
        this._activateKey2 = "8";
    }
}