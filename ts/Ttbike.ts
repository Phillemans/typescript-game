/// <reference path="Upgrades.ts" />

class Ttbike extends Upgrades{
    constructor(cost:number, distancePerClick:number) {
        super(cost,distancePerClick);
        this._name = "Ttbike";
        this._activateKey1 = "4";
        this._activateKey2 = "0";
    }
}