class UpgradeContainer {

    private _aerowheels: AeroWheels;
    private _ttwheels: TtWheels;
    private _aerobike: AeroBike;
    private _ttbike: Ttbike;
    private _upgrades: Upgrades[];

    constructor() {
        this._upgrades = [];
        this._aerowheels = new AeroWheels(100, 5);
        this._ttwheels = new TtWheels(250, 20);
        this._aerobike = new AeroBike(2000, 100);
        this._ttbike = new Ttbike(3000, 200);

        //put upgrades in array
        this._upgrades.push(this._aerowheels);
        this._upgrades.push(this._ttwheels);
        this._upgrades.push(this._aerobike);
        this._upgrades.push(this._ttbike);

        //put bike upgrades on screen
        this.createDom();
    }

    public buyItem(e: KeyboardEvent, distanceP1: number, distanceP2: number) { //decides if item can be bought
        if(e.key == "1" || "2" || "3" || "4"){
            for (let i = 0; i < this._upgrades.length; i++) {
                if (this._upgrades[i].activateP1 == e.key) {
                    if (distanceP1 >= this._upgrades[i].cost) {
                            this._upgrades[i].purchased1 = true;
                            console.log(this._upgrades[i].name + this._upgrades[i].purchased1);
                    }
                }
            }         
        }
        if(e.key == "7" || "8" || "9" || "0"){
            for (let i = 0; i < this._upgrades.length; i++) {
                if (this._upgrades[i].activateP2 == e.key) {
                    if (distanceP2 >= this._upgrades[i].cost) {
                            this._upgrades[i].purchased2 = true;
                            console.log(this._upgrades[i].purchased2)
                    }
                }
            }
        }
        this.updateUpgradeCard();
    }

    public updateUpgradeCard(){ //updates the cards with the upgrades
        for(let i = 0; i <this._upgrades.length; i++) {
            if(this._upgrades[i].purchased1){
                let card = document.getElementById(this._upgrades[i].name + 1);
                card.style.backgroundColor = "green";
                card.style.color = "white"; 
            }
            if(this._upgrades[i].purchased2){
                let card = document.getElementById(this._upgrades[i].name + 2);
                card.style.backgroundColor = "green";
                card.style.color = "white";
            }
            if(this._upgrades[i].purchased1 && this._upgrades[i].purchased2){
                let card1 = document.getElementById(this._upgrades[i].name + 1);
                card1.style.backgroundColor = "green";
                card1.style.color = "white"; 
                let card2 = document.getElementById(this._upgrades[i].name + 2);
                card2.style.backgroundColor = "green";
                card2.style.color = "white";
            }
        }
    }

    public createDom() { //creates the cards for upgrades
        for (let i = 0; i < this._upgrades.length; i++) {
            console.log(this._upgrades[i]);
            console.log(this._upgrades[i].name);
            document.getElementById("player1").innerHTML += `
                <div class="buyCard" id="${this._upgrades[i].name + 1}">
                    <img class="upgradeImage" src="./assets/images/${this._upgrades[i].name}.png" alt="temp">
                    <div class="upgradeText">
                        <h2 class="upgradeName"> ${this._upgrades[i].name}</h2>
                        <p><b>Cost: </b>${this._upgrades[i].cost} meters</p>
                        <p>Press ${this._upgrades[i].activateP1} to buy</P>
                    </div>
                </div>
            `
            document.getElementById("player2").innerHTML += `
                <div class="buyCard" id="${this._upgrades[i].name + 2}">
                    <img class="upgradeImage" src="./assets/images/${this._upgrades[i].name}.png" alt="temp">
                    <div class="upgradeText">
                        <h2 class="upgradeName"> ${this._upgrades[i].name}</h2>
                        <p><b>Cost: </b>${this._upgrades[i].cost} meters</p>
                        <p>Press ${this._upgrades[i].activateP2} to buy</P>
                    </div>
                </div>
            `
        }
    }

    get getDistanceOnClick1() {
        let returnValue = 1;
        for (let i = 0; i < this._upgrades.length; i++) {
            if (this._upgrades[i].purchased1) {
                returnValue += this._upgrades[i].distancePerClick1;
            }
        }
        return returnValue;
    }

    get getDistanceOnClick2() {
        let returnValue = 1;
        for (let i = 0; i < this._upgrades.length; i++) {
            if (this._upgrades[i].purchased2) {
                returnValue += this._upgrades[i].distancePerClick2;
            }
        }
        return returnValue;
    }

    get upgrades(){
        return this._upgrades;
    }
}