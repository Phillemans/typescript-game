class Upgrades {

    private _distancePerClick1: number;
    private _distancePerClick2: number;
    protected _cost: number;
    protected _purchased1: boolean;
    protected _purchased2: boolean;
    protected _name: string;
    protected _activateKey1: string;
    protected _activateKey2: string;

    constructor(cost:number, distancePerClick:number) {
        this._cost = cost;
        this._distancePerClick1 = distancePerClick;
        this._distancePerClick2 = distancePerClick;
        this._purchased1 = false;
        this._purchased2 = false;
    }

    set distancePerClick1(distance: number) {
        this._distancePerClick1 += distance;
    }

    set distancePerClick2(distance: number) {
        this._distancePerClick2 += distance;
    }

    set purchased1(purchased:boolean){
        this._purchased1 = purchased;
    }

    set purchased2(purchased:boolean){
        this._purchased2 = purchased;
    }

    get distancePerClick1() {
        return this._distancePerClick1;
    }

    get distancePerClick2() {
        return this._distancePerClick2;
    }

    get cost() {
        return this._cost;
    }

    get purchased1(){
        return this._purchased1;
    }
    
    get purchased2(){
        return this._purchased2;
    }

    get name(){
        return this._name;
    }

    get activateP1(){
        return this._activateKey1;
    }

    get activateP2(){
        return this._activateKey2;
    }
}