let app : any;

(function ()
{
    /**
     * run after dom is ready
     */
    let init = function () 
    {
        app = new Game();

    };
    

    window.addEventListener('load', init);

})();
